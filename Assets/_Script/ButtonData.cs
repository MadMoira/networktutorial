﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonData : MonoBehaviour {

	private HostData hostData;
	public Text buttonText;
	
	void Start() {
		GetComponent<Button>().onClick.AddListener(() => ConnectToServer());
	}
	
	private void ConnectToServer () {
		Network.Connect(hostData);
	}
	
	public void setHostData (HostData newHostData) {
		hostData = newHostData;		
	}
	
	public void setText(){
//		buttonText = GetComponent<Text>();
		buttonText.text = hostData.gameName;
	}
}
