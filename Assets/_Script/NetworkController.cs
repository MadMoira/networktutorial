﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class NetworkController : MonoBehaviour {

	public GameObject startServer;
	public GameObject refreshHostList;
	public GameObject connectButton;
	public GameObject connectPanel;
	public GameObject mainPanel;
	
	public GameObject playerCube;
	
	public Transform spawnObject;
	
	private bool refreshing = false;
	private HostData[] hostData;
	
	private string gameName = "CokeCat_Tutorial_Networking";
	
		
	// Buttons		
				
	public void StartServer () {
		Network.InitializeServer(4, 25001, !Network.HavePublicAddress());
		MasterServer.RegisterHost(gameName, "TutorialGameName", "TutorialGame");
	}
	
	public void RefreshHostList () {
		MasterServer.RequestHostList(gameName);
		refreshing = true;
	}
	
	// Functions

	void Start (){
		mainPanel.SetActive(false);
	}	
						
	void Update () {
		if (refreshing){
			if (MasterServer.PollHostList().Length > 0){
				refreshing = false;
				Debug.Log (MasterServer.PollHostList().Length);		
				hostData = MasterServer.PollHostList();
				DrawOnUI();
			}
		}
	}
	
	private void DrawOnUI () {
		mainPanel.SetActive(true);
		Vector3 localPos = new Vector3(0f, 362 - 32f, 0f);
		if ( !Network.isClient && !Network.isServer){
		
			for (int i = 0; hostData.Length > i; i++){
			
				// Posicionamiento del boton
			
				GameObject inPanelConnectButton = Instantiate(connectButton) as GameObject;
				inPanelConnectButton.transform.SetParent(connectPanel.transform, false);
				inPanelConnectButton.transform.position = connectPanel.transform.position + localPos;
								
				// Actualizacion de informacion del boton
				
				ButtonData data = inPanelConnectButton.GetComponent<ButtonData>() as ButtonData;
				data.setHostData(hostData[i]);
				data.setText();				
				
				localPos = inPanelConnectButton.transform.position + new Vector3(0f, -32f, 0f);				
			}
		}
	}
	
	private void spawn() {
		Network.Instantiate(playerCube, spawnObject.position, Quaternion.identity, 0); 
	}
	
	// Mensajes de confirmacion
	
	void OnServerInitialized() {
		Debug.Log ("Server Initialized");
		spawn ();
	}
	
	void OnConnectedToServer() {
		spawn ();
	}
	
	void OnMasterServerEvent (MasterServerEvent msEvent) {
		if (msEvent == MasterServerEvent.RegistrationSucceeded){
			Debug.Log("Server registered");
		}
	}
}
