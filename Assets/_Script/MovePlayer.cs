using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour {

	public float speed = 10f;

	void Update() {
		if ( networkView.isMine ) {
			InputColorChange ();
		}
	}

	void FixedUpdate () {
		if (networkView.isMine){		
			float moveHorizontal = Input.GetAxis ("Horizontal");
			float moveVertical = Input.GetAxis ("Vertical");
						
			Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
			
			rigidbody.AddForce (movement * speed * Time.deltaTime);
		}
//		else {
//			SyncedMovement();
//		}
	}
	
	private void SyncedMovement(){
		syncTime += Time.deltaTime;
		rigidbody.position = Vector3.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);
	}
	
	private void InputColorChange(){
		if (Input.GetKeyDown(KeyCode.R)){
			ChangeColorTo(new Vector3(Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f)));
		}
	}
	
    [RPC] void ChangeColorTo (Vector3 color) {
    	renderer.material.color = new Color(color.x, color.y, color.z, 1f);
    	
    	if (networkView.isMine){
    		networkView.RPC ("ChangeColorTo", RPCMode.OthersBuffered, color);
    	}
    }
		
	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPosition = new Vector3 (1.04f, 1.72f, 0f);
	private Vector3 syncEndPosition = Vector3.zero;
	
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info){
		Vector3 syncPosition = Vector3.zero;
		Vector3 syncVelocity = Vector3.zero;
		
		if ( stream.isWriting ) {
			syncPosition = rigidbody.position;
			stream.Serialize (ref syncPosition);
			
			syncVelocity = rigidbody.velocity;
			stream.Serialize(ref syncVelocity);
		}
		
		else {
			
			stream.Serialize (ref syncPosition);
			stream.Serialize (ref syncVelocity);
			
			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;
			
			syncEndPosition = syncPosition + syncVelocity * syncDelay;
			syncStartPosition = rigidbody.position;
						
			SyncedMovement();			
		}
	}
	
	
}
